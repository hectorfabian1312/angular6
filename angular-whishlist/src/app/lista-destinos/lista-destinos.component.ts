import { Component, OnInit, Input} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  
  descripcion: string;
  destinos: DestinoViaje[];

  nombre: string;

  constructor() { 
    this.descripcion = 'Viaje y conoce los mejores lugares del mundo';
    this.destinos = [];
  }

  ngOnInit(): void {
  }
  
  guardar(nombre:string, url:string):boolean{
    this.destinos.push(new DestinoViaje(nombre,url));
    return false;
  }

}
