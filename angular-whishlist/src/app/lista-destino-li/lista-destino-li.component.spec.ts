import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDestinoLiComponent } from './lista-destino-li.component';

describe('ListaDestinoLiComponent', () => {
  let component: ListaDestinoLiComponent;
  let fixture: ComponentFixture<ListaDestinoLiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDestinoLiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDestinoLiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
