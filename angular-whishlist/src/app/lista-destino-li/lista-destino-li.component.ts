import { Component, OnInit, Input } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destino-li',
  templateUrl: './lista-destino-li.component.html',
  styleUrls: ['./lista-destino-li.component.css']
})
export class ListaDestinoLiComponent implements OnInit {
  @Input() destino: DestinoViaje;
  constructor() { }

  ngOnInit(): void {
  }

}
